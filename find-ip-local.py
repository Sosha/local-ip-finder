#!/usr/bin/python3.5

import requests
from requests.auth import HTTPBasicAuth
from bs4 import BeautifulSoup as bs

url = requests.get("http://192.168.1.1", auth=HTTPBasicAuth("<UserName>", "<Password>"))
req = requests.get("http://192.168.1.1/status_lan.htm", auth=HTTPBasicAuth("<UserName>", "<Password>"))
soup = bs(req.text, "lxml")

for i in soup.find_all("table")[2].find_all("tr"):
    for x in i.find_all("td"):
        print (x.text, end=" | ")
    print ("\n-----------------------------------------------------------------------------")
